#! /bin/bash

patch -p1 < alt_tab.patch
patch -p1 < arrow_key.patch

if [[ "fr,us" = "$(setxkbmap -query | grep layout | awk '{print $2}')" ]]
then
	if [[ "bepo," = "$(setxkbmap -query | grep variant | awk '{print $2}')" ]]
	then
		patch -p1 < bepo.patch
		patch -p1 < config.mk.bepo.patch
	else
		patch -p1 < fr.patch
		patch -p1 < config.mk.fr.patch
	fi
fi

patch -p1 < rules.patch

patch -p1 < shiftview.patch

rm config.h
make

git checkout -- dwm.c config.def.h config.mk

